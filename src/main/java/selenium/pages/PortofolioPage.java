package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PortofolioPage {

	public WebDriver driver;
	
	public PortofolioPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public By titluriLista= By.xpath("//h4[@class='post_title']");
	
	public void hover(By locator) {
		WebElement element =  driver.findElement(locator);
		Actions actions =  new Actions(driver);
		actions.moveToElement(element).perform();
	}
	
	public String[] lista = {"15 Amazing Things About Reading in the Fall","10 Book Recommendations For Family Members","The Best business books � Financial Times","The Best Poetry Books of All Time","Five the Best Audiobooks","Book reviews: Find the best new books","How end-of-year book lists prove we lack diversity", "We need to talk about all women writers","The hottest books to warm you up this winter","Why I won�t stop buying books","Celebrity Picks:Favorite Reads of 2015","Weekend Reading","Coffee Table Books","Turn the Page: Your Next Rock �N� Roll Book Club","The Book Report: Episode 3"};
	
	
}