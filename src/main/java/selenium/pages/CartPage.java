package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {

	public WebDriver driver;
	
	public CartPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public By FilterPrice = By.xpath("//button[@class='button']");
	public By checkout = By.xpath("//a[@class='checkout-button button alt wc-forward']");
	public By incBook = By.xpath("//span[@class='q_inc']");
	public By updateCart = By.xpath("//button[@name='update_cart']");
	public By couponCode = By.xpath("//input[@class='input-text']");
	public By applyCoupon = By.xpath("//button[@name='apply_coupon']");
	public By totalPrice = By.xpath("(//span[@class='woocommerce-Price-amount amount'])[4]");
	public By messageUpdate = By.xpath("//div[@class='woocommerce-message']");
	public By messageCoupon = By.xpath("//div[@role='alert']");
	
	public void clickOnButton(By locator) {
		WebElement element = driver.findElement(locator);
		WebDriverWait wait =  new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public String getElementText(By locator) {
		
		return driver.findElement(locator).getText();

	}
}
