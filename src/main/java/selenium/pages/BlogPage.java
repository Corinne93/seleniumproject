package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BlogPage {

	public WebDriver driver;
	public WebElement dropdown;//este doar declarat- deci nu are valoare
	public Select selectDropdown;
	
	public BlogPage(WebDriver driver) {
		this.driver =  driver;
	}
	
	public By blogLink = By.linkText("BLOG");
	public By audioPost = By.xpath("(//h4[@class='post_title'])[2]");
	public By playPause = By.xpath("//div[@class='mejs-button mejs-playpause-button mejs-play']");
	//public By volume = By.xpath("//div[@class='mejs-button mejs-volume-button mejs-mute']");
	public By volumeControl = By.xpath("//a[@class='mejs-horizontal-volume-slider']");
	public By songSlider = By.cssSelector("span[class*='mejs-time-slider']");
	
	public void dragAndDropSlider(By locator, int x, int y) {
		WebElement element = driver.findElement(locator);
		Actions actions =  new Actions(driver);
		actions.dragAndDropBy(element, x, y).perform();	
	}
	
	
	public void clickOnButton(By locator) {
		WebElement element = driver.findElement(locator);
		WebDriverWait wait =  new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	
}
