package selenium;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class IsSelectedExample extends BaseTest {
	
	@Test
	public void isSelectedExample() {
		
		//se aplica doar pe elemente de tip input care au atribut type = checkbox sau radio button
		//Ex: <input type="checkbox" value="forever" id="rememberme" name="rememberme">
		
		driver.findElement(By.className("menu_user_login")).click(); 
		
		WebElement username = driver.findElement(By.id("log"));
		WebElement rememberMeCheckBox = driver.findElement(By.id("rememberme"));
		
		rememberMeCheckBox.click();
		assertTrue(rememberMeCheckBox.isSelected());
		
		
		//se aplica doar pe elemente de tip input care pot fi enable/disabled
		//pt restul elementelor va intoarce true
		username.isEnabled();
	}

}
