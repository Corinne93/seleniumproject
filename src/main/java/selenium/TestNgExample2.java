package selenium;

import org.testng.annotations.Test;

@Test
public class TestNgExample2 {

	
	public void one() {
		System.out.println("First");
	}
	
	
	public void two() {
		System.out.println("Second");
	}
	
	
	public void three() {
		System.out.println("Third");
	}
	
	
	public void four() {
		System.out.println("Fourth");
	}
	
	
	public void five() {
		System.out.println("Fifth");
	}
	
}
