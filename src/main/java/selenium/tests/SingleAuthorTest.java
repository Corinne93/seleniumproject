package selenium.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import selenium.pages.NavigationMenuPage;
import selenium.pages.SingleAuthorPage;
import slenium.utils.BaseTest;

public class SingleAuthorTest extends BaseTest{
	
	@Test
	public void singleAuthorTest() {
		
		NavigationMenuPage menu= new NavigationMenuPage(driver);
		menu.navigateTo(menu.singleAuthorLink);
				
		SingleAuthorPage skills= new SingleAuthorPage(driver);
		assertEquals(skills.skillsRead(skills.dramaSkills),"95%");
		assertEquals(skills.skillsRead(skills.biographySkills),"75%");
		assertEquals(skills.skillsRead(skills.cookbooksSkills),"82%");
		
	}

}
