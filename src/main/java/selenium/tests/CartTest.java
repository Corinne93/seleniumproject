package selenium.tests;

import static org.testng.Assert.assertEquals;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import selenium.pages.CartPage;
import selenium.pages.NavigationMenuPage;
import selenium.pages.ShopPage;
import slenium.utils.BaseTest;

public class CartTest extends BaseTest{

	@Test(priority = 1)
	public void filterByPrice(){
		NavigationMenuPage menu = new NavigationMenuPage(driver);
		menu.navigateTo(menu.shopLink);
		ShopPage shopPage = new ShopPage(driver);			
		
		shopPage.dragAndDropSlider(shopPage.firstSlider, 72, 0);		
		shopPage.dragAndDropSlider(shopPage.lastSlider, -100, 0);
		shopPage.clickOnButton(shopPage.filterButton);			
		WebElement actualText = driver.findElement(shopPage.verifyText);
		assertEquals(actualText.getText(), "Showing the single result");

		shopPage.clickOnButton(shopPage.addToCartButton);		
		shopPage.clickOnButton(shopPage.viewCart);
			
}	
		
	
	@Test(priority = 2)
	public void editCart() {
	
		CartPage cartPage = new CartPage(driver);
		driver.navigate().to("https://keybooks.ro/cart/");
		cartPage.clickOnButton(cartPage.incBook);
		cartPage.clickOnButton(cartPage.updateCart);
		
		WebElement actualMessage = driver.findElement(cartPage.messageUpdate);
		assertEquals(actualMessage.getText(), "Cart updated.");
		
		WebElement actualPrice = driver.findElement(cartPage.totalPrice);
		assertEquals(actualPrice.getText(),"$29.98");

}
	@Test(priority = 3)
	public void discountCoupon() throws InterruptedException {
		CartPage cartPage = new CartPage(driver);
		driver.findElement(cartPage.couponCode).sendKeys("WTM66DVT");
		cartPage.clickOnButton(cartPage.applyCoupon);
		
		Thread.sleep(3000);
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//WebElement alertText = driver.findElement(cartPage.messageCoupon);
		//wait.until(ExpectedConditions.textToBePresentInElement(alertText, "Coupon code applied successfully."));
		//assertEquals(cartPage.getElementText(cartPage.messageCoupon), "Coupon code applied successfully.");
		
		
		WebElement alertText = driver.findElement(cartPage.messageCoupon);
		assertEquals(alertText.getText(), "Coupon code applied successfully.");
		
		WebElement totalPrice = driver.findElement(cartPage.totalPrice);
		assertEquals(totalPrice.getText(), "$14.99");
	}
}
