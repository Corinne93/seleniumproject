package selenium.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.logging.log4j.core.appender.rolling.action.Action;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.google.errorprone.annotations.Var;

import selenium.pages.BlogPage;
import selenium.pages.NavigationMenuPage;
import selenium.pages.PortofolioPage;
import slenium.utils.BaseTest;

public class BlogTest extends BaseTest{

	//@Test
	public void PostFormats() {
		
	NavigationMenuPage menu= new NavigationMenuPage(driver);
	BlogPage blogPage = new BlogPage(driver);
	menu.hoverMenu(menu.blogLink);
	menu.navigateTo(menu.postFormatsLink);
	assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/category/post-formats/");
	//driver.findElement(blogPage.audioPost).click();	
	
	blogPage.clickOnButton(blogPage.audioPost);
	blogPage.clickOnButton(blogPage.playPause);
	blogPage.dragAndDropSlider(blogPage.songSlider, 200, 0);
	//blogPage.clickOnButton(blogPage.volume);
	blogPage.dragAndDropSlider(blogPage.volumeControl, 50 , 0);
	}
	
	@Test
	public void Portofolio2() throws InterruptedException {
		
		NavigationMenuPage menu = new NavigationMenuPage(driver);
		PortofolioPage portofolio = new PortofolioPage(driver);
		menu.hoverMenu(menu.blogLink);
		menu.hoverMenu(menu.portofolioMenu);
		menu.navigateTo(menu.porto2Submenu);
		assertEquals(driver.getCurrentUrl(),"https://keybooks.ro/category/portfolio-2-columns/");
		Thread.sleep(3000);
		//portofolio.hover(portofolio.titluriLista);
		Actions action = new Actions(driver);
		WebElement titlu = driver.findElement(portofolio.titluriLista);
		action.moveToElement(titlu).build().perform();
	
		
		List<WebElement> titluri1 = driver.findElements(portofolio.titluriLista);
	
		for(WebElement element : titluri1 ) {
			System.out.println(element);
		}
		
		assertEquals(titluri1.get(0).getText(), portofolio.lista[0]);

	
	}

	
}
