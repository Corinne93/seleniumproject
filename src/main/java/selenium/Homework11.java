package selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Homework11 extends BaseTest {

	@Test(priority = 1)
	public void discover() {
		
	
			
		WebElement discoverText = driver.findElement(By.tagName("em"));
		String actualText = discoverText.getText();
		assertEquals(actualText, "Discover");	
	
		
	}
	
	@Test(priority = 2)
	public void shop() {
		
		driver.findElement(By.linkText("BOOKS")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	}
	
	@Test(priority = 3)
	public void addbook() {
		
		driver.findElement(By.partialLinkText("galaxy")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/new-galaxy/");
	}
	
	
	@Test(priority = 4)
	public void review() {
		driver.findElement(By.id("tab-title-reviews")).click();
		
		
	}
	
	
	@Test(priority = 5)
	public void submit()  {
	    driver.findElement(By.cssSelector("input[name=submit']")).click();	
		
	}
	
		
}