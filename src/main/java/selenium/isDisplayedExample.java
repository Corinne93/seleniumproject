package selenium;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class isDisplayedExample extends BaseTest{
	
	@Test
	public void exampleIsDisplayed() {
		
		//driver.findElement(By.className("menu_user_login")).click(); //--> ca sa fie true
		
		WebElement username = driver.findElement(By.id("log"));
		WebElement password = driver.findElement(By.id("password"));
		
		assertFalse(username.isDisplayed());
		assertFalse(password.isDisplayed());
		
		driver.findElement(By.className("menu_user_login")).click(); //--> ca sa fie true
		
		assertTrue(username.isDisplayed());
		assertTrue(password.isDisplayed());
		
	}

	
	
}
