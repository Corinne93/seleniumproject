package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class ReviewHomework extends BaseTest{
	
	@Test(priority = 1)
	public void books() {
		
		driver.findElement(By.linkText("BOOKS")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	
	}
	
	@Test(priority= 2)
	public void selectBook() {
		
		driver.findElement(By.partialLinkText("Cooking")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/cooking-with-love/");
	}
	
	@Test(priority= 3)
	public void review() {
		
		driver.findElement(By.id("tab-title-reviews")).click();

	}
	
	@Test(priority=4)
	public void submitJsAlert() {
		
		driver.findElement(By.cssSelector("input[name='submit']")).click();
		driver.switchTo().alert().accept();
		
	}
	
	@Test(priority= 5)
	public void completeazaReview() {
		
		driver.findElement(By.cssSelector("a[class='star-4")).click();
		driver.findElement(By.cssSelector("textarea[name='comment'")).sendKeys("nnjhbkzcjb");
		driver.findElement(By.cssSelector("input[id='author']")).sendKeys("Corina");
		driver.findElement(By.cssSelector("input[id='email']")).sendKeys("93.corinne@gmail.com");
		driver.findElement(By.cssSelector("input[id='wp-comment-cookies-consent']")).click();
		driver.findElement(By.cssSelector("input[name='submit']")).click();

	}
	
	@Test(priority=6)
	public void awaiting() {
		
		WebElement infoMessage = driver.findElement(By.xpath("(//em[@class='woocommerce-review__awaiting-approval'])[1]"));
		assertTrue(infoMessage.getText().contains("Your review is awaiting approval"));

	}
	
	
	
	
	

}
